# PLUTO Syntax Highlighter

> This work is based on the works of Chatziargyriou Eleftheria, published under MIT license here: https://github.com/Sedictious/vscode-pluto-syntax-highlighter.

This extension provides highlighting for PLUTO (Procedure Language for Users in Test and Operations).

The PLUTO language is defined [here](https://ecss.nl/item/?glossary_id=3967).

## Manual Installation

**Installing `Pluto Syntax Highlighter` extension locally from a `.vsix` file.**

1. **Download the `.vsix` file form our [Gitlab Repo](https://gitlab.com/librecube/tools/vscodium-pluto-syntax)**: Ensure you have the `.vsix` file for the extension.

2. **Open Visual Studio Code**: Launch your VS Code editor.

3. **Install the `.vsix` file**:
   - **Using the Command Palette**:
     1. Press `Ctrl+Shift+P` (or `Cmd+Shift+P` on macOS) to open the Command Palette.
     2. Type `Extensions: Install from VSIX...` and select it.
     3. Navigate to the location where your `.vsix` file is stored and select it.
   
   - **Using Drag and Drop**:
     1. Open the Extensions view by clicking on the Extensions icon in the Activity Bar on the side of the window.
     2. Drag and drop the `.vsix` file into the Extensions view.

4. **Reload/Restart VS Code**: After installation, you might be prompted to reload the window to activate the extension. If not, you can manually reload by pressing `Ctrl+Shift+P`, typing `Reload Window`, and selecting it.

Once these steps are completed, `Pluto Syntax Highlighter` should be installed and active in your Visual Studio Code.


## Contribute

- Issue Tracker: https://gitlab.com/librecube/tools/vscodium-pluto-syntax/-/issues
- Source Code: https://gitlab.com/librecube/tools/vscodium-pluto-syntax

Helpful resources on package writing:

- [Syntax Highlight Guide](https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide)

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/!DYAKaIbRbFKLWmKDNn:matrix.org)
or via [Email](mailto:info@librecube.org).
